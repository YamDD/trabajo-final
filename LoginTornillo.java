/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package logintornillo;

/**
 *
 * @author USUARIO
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class LoginTornillo extends JFrame {
    static final String USUARIO_CORRECTO = "yam";
    static final String CONTRASEÑA_CORRECTA = "nuevo123";
    JTextField usuarioField;
    JPasswordField contraseñaField;

    public LoginTornillo() {
        super("Inicio de sesión - Eltornillo Feliz");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);

        JLabel usuarioLabel = new JLabel("Usuario:");
        usuarioField = new JTextField(10);

        JLabel contraseñaLabel = new JLabel("Contraseña:");
        contraseñaField = new JPasswordField(10);

        JButton iniciarSesionButton = new JButton("Iniciar sesión");
        iniciarSesionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                iniciarSesion();
            }
        });

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(usuarioLabel)
                        .addComponent(contraseñaLabel))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(usuarioField)
                        .addComponent(contraseñaField)
                        .addComponent(iniciarSesionButton))
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(usuarioLabel)
                        .addComponent(usuarioField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(contraseñaLabel)
                        .addComponent(contraseñaField))
                .addComponent(iniciarSesionButton)
        );

        add(panel);
        setVisible(true);
    }

    public void iniciarSesion() {
        String usuario = usuarioField.getText();
        char[] contraseñaChars = contraseñaField.getPassword();
        String contraseña = new String(contraseñaChars);

        if (usuario.equals(USUARIO_CORRECTO) && contraseña.equals(CONTRASEÑA_CORRECTA)) {
            JOptionPane.showMessageDialog(this, "Inicio de sesión exitoso.");
            // Cerramos la ventana de inicio de sesión
            dispose();
            // Abrimos la ventana de la tienda
            abrirTienda();
        } else {
            JOptionPane.showMessageDialog(this, "Usuario o contraseña incorrectos. Por favor, intente nuevamente.");
        }

        // Limpia el campo de contraseña después de usarla para mayor seguridad
        Arrays.fill(contraseñaChars, '0');
    }

    public void abrirTienda() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Tienda();
            }
        });
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                LoginTornillo loginTornillo = new LoginTornillo();
            }
        });
    }
}



