/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package logintornillo;

/**
 *
 * @author USUARIO
 */

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Tienda extends JFrame {
    JTextField productosField;

    public Tienda() {
        super("Tienda Eltornillo Feliz");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setLocationRelativeTo(null);

        JPanel panel = new JPanel();
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);

        JLabel tituloLabel = new JLabel("Catálogo de productos:");
        JLabel productosLabel = new JLabel("Productos (separados por comas):");
        productosField = new JTextField(20);

        JButton comprarButton = new JButton("Comprar");
        comprarButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                comprarProductos();
            }
        });

        JTextArea catalogoArea = new JTextArea();
        catalogoArea.setEditable(false);
        catalogoArea.setText("1. Ladrillo\n2. Clavo\n3. Cemento\n4. Arena\n5. Pintura");

        JScrollPane scrollPane = new JScrollPane(catalogoArea);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(tituloLabel)
                        .addComponent(scrollPane)
                        .addComponent(productosLabel)
                        .addComponent(productosField)
                        .addComponent(comprarButton))
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(tituloLabel)
                .addComponent(scrollPane)
                .addComponent(productosLabel)
                .addComponent(productosField)
                .addComponent(comprarButton)
        );

        add(panel);
        setVisible(true);
    }

    public void comprarProductos() {
        String entrada = productosField.getText();
        List<Integer> productos = Arrays.stream(entrada.split(","))
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .map(Integer::parseInt)
                .collect(Collectors.toList());

        StringBuilder mensaje = new StringBuilder("Compra exitosa de los productos: ");
        for (Integer producto : productos) {
            mensaje.append(obtenerNombreProducto(producto)).append(", ");
        }
        mensaje.deleteCharAt(mensaje.length() - 2); // Eliminar la coma extra al final
        JOptionPane.showMessageDialog(this, mensaje.toString());
    }

    public String obtenerNombreProducto(int numero) {
        switch (numero) {
            case 1:
                return "ladrillo";
            case 2:
                return "clavo";
            case 3:
                return "cemento";
            case 4:
                return "arena";
            case 5:
                return "pintura";
            default:
                return "producto desconocido";
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Tienda();
            }
        });
    }
}



